package com.ozz.ozzmall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OzzmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OzzmallOrderApplication.class, args);
    }

}
