package com.ozz.ozzmall.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OzzmallUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(OzzmallUserApplication.class, args);
    }

}
