package com.ozz.ozzmall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OzzmallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(OzzmallWareApplication.class, args);
    }

}
