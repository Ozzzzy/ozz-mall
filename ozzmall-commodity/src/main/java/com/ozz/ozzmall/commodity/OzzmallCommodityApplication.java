package com.ozz.ozzmall.commodity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OzzmallCommodityApplication {

    public static void main(String[] args) {
        SpringApplication.run(OzzmallCommodityApplication.class, args);
    }

}
