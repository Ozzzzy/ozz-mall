package com.ozz.ozzmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OzzmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(OzzmallCouponApplication.class, args);
    }

}
